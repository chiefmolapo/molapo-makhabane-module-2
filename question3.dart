void main(List<String> args) {
  var p = mtnApp('khula', 'Agriculture', 'Karidas Tshintsholo,', 2018);

  print("${p.nameOfApp} , ${p.category} ${p.developer} and ${p.year}");
  p.printCapital('khula');
}

class mtnApp {
  String nameOfApp;
  String category;
  String developer;
  int year;
  mtnApp(this.nameOfApp, this.category, this.developer, this.year);

  void printCapital(String name) {
    name = nameOfApp.toUpperCase();
    print("$name");
  }
}
